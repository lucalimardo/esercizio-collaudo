# Esercizio direttiva #

Clona il progetto in locale da questo repository.
```
https://lucalimardo@bitbucket.org/lucalimardo/esercizio-collaudo.git
```

Completa la direttiva in modo che restituisca un messaggio (alert, console.log, html, ecc..) quando i testi dei due input coincidono.