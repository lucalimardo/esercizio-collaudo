(function(angular){
angular
  .module('app', [])
  .controller('AppController', function($scope){
    $scope.inputone = 'Ciao';
    $scope.inputtwo = 'Mondo';
  })
  .directive('matchDepts', function(){
    return {
      restrict:'A',
      require:'ngModel',
      link: function(scope, element, attrs, controller){
      }
    };
  });
})(window.angular)
